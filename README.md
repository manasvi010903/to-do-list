# Todo List

A simple Todo List application built using HTML, CSS, and JavaScript.

## Features
- Add new tasks
- Mark tasks as completed
- Delete tasks


## Installation
1. Clone the repository:
2. Open `index.html` in your browser to see the application in action.

## Usage
1. Type a task in the input field.
2. Click the "Add" button to add the task to the list.
3. Click on a task to mark it as completed.
4. Click the "X" button next to a task to delete it.

